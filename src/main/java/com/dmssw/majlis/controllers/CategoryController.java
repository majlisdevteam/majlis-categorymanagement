/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.majlis.controllers;

import com.dmssw.majlis.config.AppParams;
import com.dmssw.orm.controllers.DbCon;
import com.dmssw.orm.controllers.ORMConnection;
import static com.dmssw.orm.controllers.ORMConnection.logger;
import com.dmssw.orm.models.MajlisCategory;
import com.dmssw.orm.models.MajlisMdCode;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.imageio.ImageIO;
import org.hibernate.Session;

/**
 *
 * @author Dasun Chathuranga
 */
@Stateless
@LocalBean
@TransactionManagement(TransactionManagementType.BEAN)
public class CategoryController {

    private static Logger LOGGER = Logger.getLogger("InfoLogging");
    
    public ResponseData getCategories(int categoryId,
            String categoryTitle,
            String categoryDescription,
            String userInserted) {

        ORMConnection conn = new ORMConnection();

        ResponseData rd = new ResponseData();

        int result = -1;

        try {

//            String where =(categoryId == 0 ? "C.categoryId=C.categoryId" : "AND C.categoryId = '" + categoryId + "'");
//            where +=(categoryTitle.equalsIgnoreCase("all") ? " IFNULL(C.categoryTitle,'AA') = IFNULL(C.categoryTitle,'AA')" : " UPPER(C.categoryTitle) LIKE UPPER('%" + categoryTitle + "%') ");
//            where +=(categoryDescription.equalsIgnoreCase("all") ? " IFNULL(C.categoryDescription,'AA') = IFNULL(C.categoryDescription,'AA')" : " UPPER(C.categoryDescription) LIKE UPPER('%" + categoryDescription + "%') ");
//            where +=(userInserted.equalsIgnoreCase("all") ? " IFNULL(C.userInserted,'AA') = IFNULL(C.userInserted,'AA')" : " UPPER(C.userInserted) LIKE UPPER('%" + userInserted + "%') ");
//            
            String where = (categoryId == 0 ? " WHERE C.categoryId=C.categoryId" : " WHERE C.categoryId = '" + categoryId + "'");
            where += (categoryTitle.equalsIgnoreCase("all") ? " AND COALESCE(C.categoryTitle,'AA') = COALESCE(C.categoryTitle,'AA') " : " AND C.categoryTitle LIKE '%" + categoryTitle + "%' ");
            where += (categoryDescription.equalsIgnoreCase("all") ? " AND COALESCE(C.categoryDescription,'AA')  = COALESCE(C.categoryDescription,'AA') " : " AND C.categoryDescription LIKE '%" + categoryDescription + "%' ");
            where += (userInserted.equalsIgnoreCase("all") ? " AND COALESCE(C.userInserted,'AA')  = COALESCE(C.userInserted,'AA') " : " AND C.userInserted LIKE '%" + userInserted + "%' ");

//            String hql = "SELECT C from MajlisCategory C WHERE C.categoryId = " +category_id  + " AND C.categoryTitle= " +category_title+ " AND C.categoryDescription = "+category_description+""
//                    + "AND C.userInserted = "+user_inserted+"";
            String hql = "SELECT C from MajlisCategory C " + where;

            List<Object> li = conn.hqlGetResults(hql);

            List<MajlisCategory> catList = new ArrayList<>();

            for (Object obj : li) {
                MajlisCategory cat = (MajlisCategory) obj;

                catList.add(cat);

            }
            rd.setResponseData(catList);

            result = 1;
        } catch (Exception e) {
            result = 999;
            e.printStackTrace();
        }

        rd.setResponseCode(result);

        return rd;

    }

//    public ResponseData createCategory(MajlisCategory catList) {
//
////        System.out.println("Create Category zzz");
//
//        ORMConnection orm = new ORMConnection();
//
//        Session session = orm.beginTransaction();
//
//        ResponseData rd = new ResponseData();
//        int result = -1;
//        try {
//
//            orm.createObject(session, catList);
////            System.out.println("CAT List" + catList.getCategoryId());
//            orm.commitObject(session);
//            
//            rd.setResponseData(catList);
//            result = 1;
//        } catch (Exception e) {
//            result = 999;
//            e.printStackTrace();
//        }
//        
//        rd.setResponseData(result);
//        return rd;
//    }
//    
     
    
    public ResponseData validateCategoryIcon(MajlisCategory mCategory){
      
        String base64Img = mCategory.getCategoryIconPath();
        int groupId = mCategory.getCategoryId();
        
        ResponseData rs = new ResponseData();
        
        if ((base64Img != null) && (base64Img.length() > 0)) {
            try {

                byte[] decodedBytes = Base64.getDecoder().decode(base64Img);

                final BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(decodedBytes));

                String image_URL = AppParams.IMG_PATH + "/" + groupId + "_GroupIcons" + ".jpg";
                System.out.println("Stack Graphic Image Path: " + image_URL);
                try {
                    File f = new File(image_URL);
                    ImageIO.write(bufferedImage, "PNG", f);
                    mCategory.setCategoryIconPath(image_URL);

                    rs.setResponseData("successfull");
                    rs.setResponseCode(1);
                    rs.setResponseData(1);

                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error Saving Image : " + e.getMessage());

                    rs.setResponseData("Error Saving Image");
                    rs.setResponseCode(-1);
                    rs.setResponseData(0);
                }
            } catch (Exception ex) {
                System.out.println("Error in ValidateGroupIcon: " + ex.getMessage());
                rs.setResponseData("Error in ValidateGroupIcon");
                rs.setResponseCode(-1);
                rs.setResponseData(0);

            }

        }
        return rs;
    }
    
    
   //======================================================================================================================
    
    /**
     * Retrieves all the database records of a majlis_category table.
     *
     * @return ResponseData object with response code, 1 if successfull else 999
     * and list of categories as response object.
     */
    public ResponseData getCategoryList(int start, int limit) {

        ResponseData rd = new ResponseData();
        int result = -1;
        try {
            LOGGER.info("Inside get category list..");
            ORMConnection conn = new ORMConnection();

            MajlisCategory majlisCategory = new MajlisCategory();
            List<MajlisCategory> majlisCategoryList = new ArrayList();

            String hql = "SELECT m FROM MajlisCategory m";

            List<Object> resultList = conn.hqlGetResultsWithLimit(hql, start, limit);

            for (Object obj : resultList) {
                majlisCategory = (MajlisCategory) obj;
                majlisCategory.setCategoryIconPath(validateCategoryIconPath(majlisCategory.getCategoryIconPath()));
                majlisCategoryList.add(majlisCategory);
            }

            LOGGER.info("Success");
            result = 1;

            rd.setResponseData(resultList);
        } catch (Exception e) {
            result = 999;
            e.printStackTrace();

        }
        rd.setResponseCode(result);
        return rd;
    }

    /**
     * Retrieves majlis_category object specified by category id
     *
     * @param categoryId - unique id for a category.
     * @return
     */
    public ResponseData getCategoryDetail(int categoryId) {

        ResponseData responseDate = new ResponseData();

        MajlisCategory majlisCategory = new MajlisCategory();
        List<Object> resultList;
        List<MajlisCategory> majlisCategoryList = new ArrayList<MajlisCategory>();
        int result = -1;

        try {
            ORMConnection conn = new ORMConnection();

            String hql = "SELECT m FROM MajlisCategory m WHERE m.categoryId = " + categoryId;

            resultList = conn.hqlGetResults(hql);

            for (Object object : resultList) {
                majlisCategory = (MajlisCategory) object;
                majlisCategory.setCategoryIconPath(validateCategoryIconPath(majlisCategory.getCategoryIconPath()));
                majlisCategoryList.add(majlisCategory);
            }

            result = 1;
            responseDate.setResponseData(majlisCategoryList);

        } catch (Exception e) {
            result = 999;
            LOGGER.info("Error while getting data...");
            e.printStackTrace();
        }

        responseDate.setResponseCode(result);
        return responseDate;

    }

    /**
     * Insert a new 'majlis_category' database record
     *
     * @param majlisCategory
     * @return
     */
    public ResponseData createCategory(MajlisCategory majlisCategory) {

        ResponseData responseData = new ResponseData();
        int result = -1;
        int categoryId;

        ORMConnection conn = new ORMConnection();
        Session session = conn.beginTransaction();

        try {

            if ((majlisCategory.getCategoryTitle()) != null && (majlisCategory.getCategoryIconPath()) != null
                    && (majlisCategory.getCategoryDescription()) != null && (majlisCategory.getCategoryStatus()) != null) {

                categoryId = conn.createObject(session, majlisCategory);

                majlisCategory.setCategoryId(categoryId);
                majlisCategory.setCategorySystemId(generateSystemId(categoryId));
                majlisCategory.setCategoryIconPath(convertIconPath(majlisCategory.getCategoryIconPath(), categoryId));

                conn.updateObject(session, majlisCategory);
                conn.commitObject(session);
                result = 1;
                responseData.setResponseData(majlisCategory);

            } else {
                result = 999;
                LOGGER.info("Not successfull");
                responseData.setResponseData(majlisCategory);//for testing
            }

        } catch (Exception ex) {

            result = 999;
            ex.printStackTrace();

        }

        responseData.setResponseCode(result);
        return responseData;

    }

    /**
     * Update majlis category database record
     *
     * @param majlisCategory
     * @return
     */
    public ResponseData updateCategory(MajlisCategory majlisCategory) {

        ResponseData responseData = new ResponseData();
        int result = -1;

        ORMConnection conn = new ORMConnection();
        Session session = conn.beginTransaction();

        Integer categoryId = majlisCategory.getCategoryId();
        try {
            if (categoryId != null && (majlisCategory.getCategoryTitle()) != null && (majlisCategory.getCategoryIconPath() != null)
                    && (majlisCategory.getCategoryDescription()) != null && (majlisCategory.getCategoryStatus() != null)) {

                try {
                    majlisCategory.setCategoryIconPath(convertIconPath(majlisCategory.getCategoryIconPath(), categoryId));

                    conn.updateObject(session, majlisCategory);
                    conn.commitObject(session);
                    result = 1;
                    responseData.setResponseData(majlisCategory);//For teting
                } catch (Exception ex) {
                    result = 999;
                    LOGGER.info("Exception ... !");
                    ex.printStackTrace();
                }

            } else {
                LOGGER.info("Not a Valid Record...");
                result = 999;
            }
        } catch (Exception ex) {
            result = 999;
            LOGGER.info("Exception");
            ex.printStackTrace();
        }

        responseData.setResponseCode(result);
        return responseData;
    }

    /**
     * Returns category detail list for a given user ID
     *
     * @param userId
     * @param start
     * @param limit
     * @return
     */
    public ResponseData getCategorybyUser(int userId, int start, int limit) {
        ResponseData responseData = new ResponseData();
        MajlisCategory majlisCategory = new MajlisCategory();
        int result = -1;
        List<MajlisCategory> majlisCategoryList = new ArrayList<>();

        try {

            DbCon dbConnection = new DbCon();
            Connection connection = dbConnection.getCon();

            String sql = "SELECT * FROM majlis_mobile_users_category mmuc,majlis_category mc WHERE mmuc.CATEGORY_ID=mc.CATEGORY_ID AND mmuc.USER_ID= " +
                    userId + " LIMIT " + limit + " OFFSET " + start;
            ResultSet rs = dbConnection.search(connection, sql);

            LOGGER.info(sql);
            while (rs.next()) {
                majlisCategory.setCategoryDescription(rs.getString("CATEGORY_DESCRIPTION"));
                majlisCategory.setCategoryIconPath(validateCategoryIconPath(rs.getString("CATEGORY_ICON_PATH")));
                majlisCategory.setCategoryId(rs.getInt("CATEGORY_ID"));
                majlisCategory.setCategorySystemId(rs.getString("CATEGORY_SYSTEM_ID"));
                majlisCategory.setCategoryTitle(rs.getString("CATEGORY_TITLE"));
                majlisCategory.setDateInserted(rs.getDate("DATE_INSERTED"));
                majlisCategory.setDateModified(rs.getDate("DATE_MODIFIED"));
                majlisCategory.setUserInserted(rs.getString("USER_INSERTED"));
                majlisCategory.setUserModified(rs.getString("USER_MODIFIED"));
                
                LOGGER.info(majlisCategory.getCategoryTitle());
                LOGGER.info("Inside While");

                majlisCategoryList.add(majlisCategory);
            }
            result = 1;
            responseData.setResponseData(majlisCategoryList);

        } catch (Exception ex) {

            result = 999;
            LOGGER.info("Exception ...");
            ex.printStackTrace();
        }

        responseData.setResponseCode(result);
        return responseData;
    }

    public ResponseData getAllStatus() {

        ResponseData rd = new ResponseData();

        DbCon db = new DbCon();

        Connection conn = db.getCon();

        ORMConnection orm = new ORMConnection();

        List<MajlisMdCode> mdList = new ArrayList<>();

        int result = -1;

        try {

            String sql = "SELECT U FROM MajlisMdCode U WHERE U.codeType='CATEGORY' AND U.codeSubType='CATEGORY_STATUS' "
                    + "AND U.codeLocale='" + AppParams.LOCALE + "'";

            List<Object> resultSet = orm.hqlGetResults(sql);

            for (Object object : resultSet) {
                MajlisMdCode md = (MajlisMdCode) object;

                mdList.add(md);

            }
            result = 1;
            rd.setResponseData(mdList);

        } catch (Exception e) {
            e.printStackTrace();
            result = 999;
            logger.error("getAllUserActiveStatus error " + e.getMessage());
        }

        rd.setResponseCode(result);

        return rd;
    }

    /**
     * This method creates a new system ID for a new category
     *
     * @param categoryId
     * @return the new system Id
     */
    private String generateSystemId(int categoryId) {
        String systemId;
        Date currentDate = new Date();

        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);
        String year = String.valueOf(cal.get(Calendar.YEAR));
        String month = String.valueOf(cal.get(Calendar.MONTH) + 1);
        systemId = year + month + categoryId + "";
        return systemId;

    }

    /**
     * This method converts base64 string to the icon image and set actual URL.
     * iVBORw0KGgoAAAANSUhEUgAAAHkAAAB5CAMAAAAqJH57AAAAV1BMVEX///9mZmZjY2NfX19bW1tXV1d5eXmVlZWenp7Z2dltbW34+Pjx8fHHx8fp6ens7Oy7u7uysrKkpKSDg4Pi4uJycnKJiYnR0dGqqqqPj4/BwcFSUlJNTU2CWPSYAAADtklEQVRoge2b65aqMAyFaVLKXUCu6rz/cx5AZwTUsZF96jpzZi9/svhW2pAmafS8X/2rSqOsD2rn2CQKG9KsO9fgzPhMSil2TK6NHrGDKHIK7k8X7kB2uc9Jq9VVO3fg3PCVS03yHrCi1hk4qeZgpXtn5MMCrLQz1+71AkzG1TZHvlqaXDgCJ4pW5NQRebXW7vyrXlmslCuTQ16ZfHQETuMl2F0UOa6+KOUscAaLbSZ2FkRWi82ZK7BXL/yLXXnXoM6fL7U7iz2vuDoYx05zoC8ycesqgszJxNq4TnSLD619Pw7dJpujkizLur+c7yV5XUdR/WQz07orykHHrM4h1LpsTUyKiOIq7B68M+36KqZhz3n8DU/uo41OtysNa6IvJ9bqUNwscF4EsWaax9LhyabfkPPvAub1ETwUbdUcnmYt65tz+gwPX/WCnvjOG8/w8rzsUaDuYi8BhsqXDG70w1cOBp1Mfwzj02PsJF3Jtzv7xpYL/HYr7jwlri3L+wstl/TYLr9ZaSlaZHWBsnhCx/YuHuEsntCVbYKWIi0e9WH7cVXPXVYi0rbgArvW3Nhuc45da32w5N6WLNvEe2vwbZW2CRxag70AaTK39gXPDsgdYoggOYHF61EnSVLaALeZAwE4QprsSxLBHkgWmewZ4GKLmnIpEEyNxGTk8SiIXh42I5BdK+yBZBKVOC1wn2MJ2ItxZKpEZP/5G20l+5qhZJFr/4/k5GfY/DYPe99XpdTbyCRqVQDBwq4zsqIikYsF0DRfcrkAzXm1ZLkzaK/ACMg1tID1BSlgigTLggky6x2MFtyqQCtJUTQ5gsn2F+HYul10YiFryQlt3TTAdkkk6A5NVtryMiuPn79LKG7s6hxkmXER6b2Ni2fYDuBZrMvnS56ivfusk8WgCdy7J9lEs/VYDAZsM/yQYE+Ns+xu5MFd5kmx1TeNPaQn2QYyZM/iLNuxxBS93PYJMNpo+4QMbDTZXyiAjZa0IVNgk0hYSSO/aV92/42r7Vg4oQdLBUnWC/Rw98Dyee4Ek5xILqw+tUN8WWReGStB5EUvTuhtL7Ks73/X2pqSCbtxM+W0CS27Q1mq24ImtWVkakPjhDb+S+FxQU2s9WpCagneOit3F03MTbUvjseyNeruEA8BpiG79QDRNPt1HTzbdftxOmz5DDeIP4SkZsYm7Zvy5tjblcafP+QHoOHirBk3dBy2o0P2IBzmRUXTvhP7BjgOGfVt07Rl9K0pSVSEh8OhR49hJu7+1vKrn6M/XKoyZiVvmakAAAAASUVORK5CYII=
     *
     * @param base64Img
     * @param groupId
     * @return the path of the icon location
     */
    private String convertIconPath(String base64Img, int categoryId) {
        String iconPath = null;

        if ((base64Img != null) && (base64Img.length() > 0)) {

            try {

                byte[] decodedBytes = Base64.getMimeDecoder().decode(base64Img);
                BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(decodedBytes));

                if (bufferedImage != null) {

                    String path = AppParams.IMG_PATH_CATEGORY+"/" + categoryId + "_CategoryIcons" + ".jpg";
                    
                    path = path.replace("\\", "/");
                    iconPath = path;

                    File f = new File(AppParams.IMG_PATH+"/" + path);
                    ImageIO.write(bufferedImage, "png", f);

                } else {
                    LOGGER.info("NO image to write");

                }

            } catch (IOException e) {

                LOGGER.info("Error in Streaming...");
                e.printStackTrace();
            }

        } else {
            LOGGER.info("Invalid Image...");
        }

        return iconPath;

    }

    private String validateCategoryIconPath(String path) {

        String ipath = "";
        if (path != null) {

            return  AppParams.MEDIA_SERVER + path;

        }
        return ipath;
    }

    
    
}
