/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.majlis.controllers;
import java.util.List;

/**
 *
 * @version 1.0 / 15 March 2017
 * @author Sandali Kaushalya
 */
public class ResponseData {

    private Object responseCode;
    
    private Object responseData;

    public ResponseData() {
    }

    public ResponseData(Object responseCode, Object responseData) {
        this.responseCode = responseCode;
        this.responseData = responseData;
    }

    
    public Object getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Object responseCode) {
        this.responseCode = responseCode;
    }

    public Object getResponseData() {
        return responseData;
    }

    public void setResponseData(Object responseData) {
        this.responseData = responseData;
    }

    
    
    
}
