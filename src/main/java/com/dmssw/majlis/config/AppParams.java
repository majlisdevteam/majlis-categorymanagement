/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.majlis.config;

import java.io.FileInputStream;
import java.util.Properties;

/**
 *
 * @version 1.0 / 20 March 2017
 * @author Sandali Kaushalya
 */
public class AppParams {

    static Properties props = new Properties();
    static String wspath = System.getenv("WS_HOME");

    static String pathset = wspath.replace("\\", "/");
    static String ippath = pathset + "/Majlis_Conn.properties";

    public static final String SYSTEM_NAME;
    public static final String CONNECTION_MYSQL;
    public static final String DRIVER;

    public static final String MEDIA_SERVER;
    public static final String IMG_PATH_GROUP;
     public static final String IMG_PATH_CATEGORY;

    public static final String DB_USER_NAME;
    public static final String DB_PASSWORD;

    public static final String LDAP_PROTOCAL;
    public static final String LDAP_HOST;
    public static final String LDAP_SYSTEM_SID;
    public static final String LDAP_WS_PORT;
    public static final String LDAP_WS_CONTEXT_URL;
    public static final String LoggedInDN;
    public static final String LOCALE;
    
   // static final String DIRECTORY = "D:\\wildfly-10.1.0.Final\\standalone\\data\\Images";
    //public static String UPLOAD_DIRECTORY = DIRECTORY.replace("\\", "/");

    public static final String IMG_PATH;
    
    public static final String LDAP_WS_URL;

    static {

        System.out.println("Majlis property path " + ippath);

        try {

            props.load(new FileInputStream(ippath));

        } catch (Exception e) {
            System.out.println("Majlis cannot read properties.....");
            e.printStackTrace();
        }

        MEDIA_SERVER = props.getProperty("MEDIA_SERVER");
        IMG_PATH_GROUP = props.getProperty("IMG_PATH_GROUP");
        
        LOCALE =props.getProperty("LOCALE");

        SYSTEM_NAME = props.getProperty("SYSTEM_NAME");
        IMG_PATH = props.getProperty("IMG_PATH");
        IMG_PATH_CATEGORY = props.getProperty("IMG_PATH_CATEGORY");
        
        DRIVER = props.getProperty("DRIVER");
        CONNECTION_MYSQL = props.getProperty("CONNECTION_MYSQL");
        DB_USER_NAME = props.getProperty("DB_USER_NAME");
        DB_PASSWORD = props.getProperty("DB_PASSWORD");

        LDAP_PROTOCAL = props.getProperty("LDAP_PROTOCAL");
        LDAP_HOST = props.getProperty("LDAP_HOST");
        LDAP_SYSTEM_SID = props.getProperty("LDAP_SYSTEM_SID");
        LDAP_WS_PORT = props.getProperty("LDAP_WS_PORT");
        LDAP_WS_CONTEXT_URL = props.getProperty("LDAP_WS_CONTEXT_URL");
        LoggedInDN = props.getProperty("LoggedInDN");
        //LOCALE = "EN";
        LDAP_WS_URL = LDAP_PROTOCAL + "://" + LDAP_HOST + ":" + LDAP_WS_PORT + "/" + LDAP_WS_CONTEXT_URL;

    }

}
