/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.majlis.services;

import com.dmssw.majlis.controllers.CategoryController;
import com.dmssw.orm.models.MajlisCategory;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Dasun Chathuranga
 */
@Path("/categories")
public class Categories {

    @EJB
    CategoryController categoryController;

    @GET
    @Path("/getCategoryDetails")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getCategoryDetails(@QueryParam("categoryId") @DefaultValue("0") Integer categoryId,
            @QueryParam("categoryTitle") @DefaultValue("all") String categoryTitle,
            @QueryParam("categoryDescription") @DefaultValue("all") String categoryDescription,
            @QueryParam("userInserted") @DefaultValue("all") String userInserted) {

        return Response.status(Response.Status.OK).entity(categoryController.getCategories(categoryId, categoryTitle, categoryDescription, userInserted)).build();
    }

  
    @POST
    @Path("/validateCategoryIcon")
    @Produces({MediaType.APPLICATION_JSON})
    public Response validateCategoryIcon(MajlisCategory mCategory) {

        CategoryController catController = new CategoryController();
        return Response.status(Response.Status.OK).entity(catController.validateCategoryIcon(mCategory)).build();
    }
    
    
    //===========================================================================================================
    
    /**
     * /Majlis-CategoryManagement-1.0/service/categories/getCategoryList
     * @param start
     * @param limit
     * @return 
     */
    @GET
    @Path("/getCategoryList")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getCategoryList(@QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("25") int limit) {

        return Response.status(Response.Status.OK).entity(categoryController.getCategoryList(start, limit)).build();
    }

    /**
     * /Majlis-CategoryManagement-1.0/service/categories/getCategoryDetail
     *
     * @param categoryId
     * @param start - starting index of the record.
     * @param limit - number of records for a service call
     * @return
     */
    @GET
    @Path("/getCategoryDetail")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getCategoryDetail(@QueryParam("categoryId") @DefaultValue("0") int categoryId,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("25") int limit) {

        return Response.status(Response.Status.OK).entity(categoryController.getCategoryDetail(categoryId)).build();
    }
    
    /**
     * /Majlis-CategoryManagement-1.0/service/categories/getCategoryDetailbyUser`
     * @param userId
     * @param start
     * @param limit
     * @return 
     */
    @GET
    @Path("/getCategoryDetailbyUser")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getCategoryDetailbyUser(@QueryParam("userId") @DefaultValue("0") int userId,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("25") int limit) {

        return Response.status(Response.Status.OK).entity(categoryController.getCategorybyUser(userId, start, limit)).build();
    }

    /**
     *  /Majlis-CategoryManagement-1.0/service/categories/createCategory
     * @param majlisCategory
     * @return 
     */
    @POST
    @Path("/createCategory")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response createCategory(MajlisCategory majlisCategory) {

        return Response.status(Response.Status.OK).entity(categoryController.createCategory(majlisCategory)).build();
    }

    /**
     *  /Majlis-CategoryManagement-1.0/service/categories/createCategory/updateCategory
     * @param majlisCategory
     * @return 
     */
    @POST
    @Path("/updateCategory")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response upateCategory(MajlisCategory majlisCategory) {

        return Response.status(Response.Status.OK).entity(categoryController.updateCategory(majlisCategory)).build();
    }
    
    /**
     * /Majlis-CategoryManagement-1.0/service/categories/createCategory/getAllCategoryStatus
     * @return 
     */
    @GET
    @Path("/getAllCategoryStatus")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getAllCategoryStatus() {

        return Response.status(Response.Status.OK).entity(categoryController.getAllStatus()).build();
    }
}
