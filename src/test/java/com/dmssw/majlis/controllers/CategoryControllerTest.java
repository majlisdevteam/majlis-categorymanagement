///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//
//package com.dmssw.majlis.controllers;
//
//import com.dmssw.orm.models.MajlisCategory;
//import java.util.ArrayList;
//import java.util.List;
//import javax.ejb.embeddable.EJBContainer;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import static org.junit.Assert.*;
//
///**
// *
// * @author Dasun Chathuranga
// */
//public class CategoryControllerTest {
//    
//    public CategoryControllerTest() {
//    }
//    
//    @BeforeClass
//    public static void setUpClass() {
//    }
//    
//    @AfterClass
//    public static void tearDownClass() {
//    }
//    
//    @Before
//    public void setUp() {
//    }
//    
//    @After
//    public void tearDown() {
//    }
//
//    /**
//     * Test of getCategories method, of class CategoryController.
//     */
////    @Test
////    public void testGetCategories() throws Exception {
////        System.out.println("getCategories");
////        int categoryId = 0;
////        String categoryTitle = "";
////        String categoryDescription = "";
////        String userInserted = "";
////        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
////        CategoryController instance = (CategoryController)container.getContext().lookup("java:global/classes/CategoryController");
////        ResponseData expResult = null;
////        ResponseData result = instance.getCategories(categoryId, categoryTitle, categoryDescription, userInserted);
////        assertEquals(expResult, result);
////        container.close();
////        // TODO review the generated test code and remove the default call to fail.
////        fail("The test case is a prototype.");
////    }
//    
//    @Test
//    public void testGetCategories() throws Exception {
//        System.out.println("getCategories");
//        MajlisCategory m_cat = new MajlisCategory();
//        m_cat.setCategoryId(8);
//        m_cat.setCategorySystemId(null);
//        m_cat.setCategoryTitle(null);
//        m_cat.setCategoryIconPath(null);
//        m_cat.setCategoryDescription(null);
//        m_cat.setUserInserted(null);
//        m_cat.setDateInserted(null);
//        m_cat.setUserModified(null);
//        m_cat.setDateModified(null);
//        m_cat.setCategoryStatus(null);
//        
//       ResponseData rd = new ResponseData();
//        List<Object> catList = new ArrayList<>();
//        catList.add(m_cat);
//       rd.setResponseData(catList);
//       
//       
//        int categoryId = 0;
//        String categoryTitle = "";
//        String categoryDescription = "";
//        String userInserted = "";
//        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
//        CategoryController instance = (CategoryController)container.getContext().lookup("java:global/classes/CategoryController");
//        ResponseData expResult = rd;
//        ResponseData result = instance.getCategories(categoryId, categoryTitle, categoryDescription, userInserted);
//        assertEquals(expResult, result);
//        container.close();
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of createCategory method, of class CategoryController.
//     */
////    @Test
////    public void testCreateCategory() throws Exception {
////        System.out.println("createCategory");
////        MajlisCategory catList = null;
////        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
////        CategoryController instance = (CategoryController)container.getContext().lookup("java:global/classes/CategoryController");
////        ResponseData expResult = null;
////        ResponseData result = instance.createCategory(catList);
////        assertEquals(expResult, result);
////        container.close();
////        // TODO review the generated test code and remove the default call to fail.
////        fail("The test case is a prototype.");
////    }
//
//    @Test
//    public void testCreateCategory() throws Exception {
//        System.out.println("createCategory");
//        MajlisCategory catList = new MajlisCategory();
//        catList.setCategoryId(11);
//        catList.setCategorySystemId(null);
//        catList.setCategoryTitle("");
//        catList.setCategoryIconPath(null);
//        catList.setCategoryDescription("");
//        catList.setUserInserted("");
//        catList.setDateInserted(null);
//        catList.setUserModified("");
//        catList.setCategoryStatus(null);
//        
//        CategoryController instance = new CategoryController();
//        ResponseData rd = new ResponseData();
//        rd.setResponseData(null);
//        rd.setResponseCode(1);
//        ResponseData expResult = rd;
//        ResponseData result = instance.createCategory(catList);
//   
//        boolean resultBool = (result.getResponseCode()== rd.getResponseCode());      
//
//        
//        assertFalse("category created", resultBool);
//        fail("The test case is a prototype.");
//    }
//    
//    /**
//     * Test of validateCategoryIcon method, of class CategoryController.
//     */
//    @Test
//    public void testValidateCategoryIcon() throws Exception {
//        System.out.println("validateCategoryIcon");
//        MajlisCategory mCategory = null;
//        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
//        CategoryController instance = (CategoryController)container.getContext().lookup("java:global/classes/CategoryController");
//        ResponseData expResult = null;
//        ResponseData result = instance.validateCategoryIcon(mCategory);
//        assertEquals(expResult, result);
//        container.close();
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//    
//}
